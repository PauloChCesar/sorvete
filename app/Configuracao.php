<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Eloquent;

class Configuracao extends Eloquent
{
    public $table = "produtos_configuracoes";

    protected $fillable = [
        'id', 'idProduto', 'titulo', 'ativo', 'dtInclusao', 'dtExclusao'
    ];

    public $timestamps = false;

    protected $dates = ['dtInclusao','dtExclusao'];
}
