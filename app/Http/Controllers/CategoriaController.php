<?php

namespace App\Http\Controllers;

use App\Categoria;
use Illuminate\Http\Request;

class CategoriaController extends Controller
{
    public function index(Request $msg)
    {
        $msg = $msg->input('msg');

        $registros = Categoria::all();

        return view('categorias.index',compact('registros', 'msg'));
    }

    public function adicionar()
    {
        return view('categorias.adicionar');
    }

    public function salvar(Request $req)
    {
        $dados = $req->all();

        Categoria::create($dados);

        return redirect()->route('categorias', 'msg=ok');
    }



    public function editar($id)
    {
        $registro = Categoria::find($id);

        return view('categorias.editar', compact('registro'));
    }

    public function atualizar(Request $req, $id)
    {
        $dados = $req->all();

        Categoria::find($id)->update($dados);

        return redirect()->route('categorias', 'msg=ok');
    }



    public function excluir($id)
    {
        $dado = Categoria::find($id);
        $dado->delete();

        return redirect()->route('categorias', 'msg=del');
    }
}