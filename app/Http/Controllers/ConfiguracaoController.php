<?php

namespace App\Http\Controllers;

use App\Configuracao;
use Illuminate\Http\Request;
use App\Registro;
use DB;
use App\Quotation;

class ConfiguracaoController extends Controller
{
    public function index($idProduto, Request $msg)
    {
        $msg = $msg->input('msg');

        $registros = Configuracao::where('idProduto', '=', $idProduto)->get();

        if($registros->count()<1){
            DB::insert("insert into gat_sistema.produtos_configuracoes(idProduto, titulo, tipo) select id, 'Cores', 1 from gat_sistema.produtos where ativo = 'sim' and id = ".$idProduto);
            DB::insert("insert into gat_sistema.produtos_configuracoes(idProduto, titulo, tipo) select id, 'Dimensões', 2 from gat_sistema.produtos where ativo = 'sim' and id = ".$idProduto);
            DB::insert("insert into gat_sistema.produtos_configuracoes(idProduto, titulo, tipo) select id, 'Especificações técnicas', 3 from gat_sistema.produtos where ativo = 'sim' and id = ".$idProduto);
            DB::insert("insert into gat_sistema.produtos_configuracoes(idProduto, titulo, tipo) select id, 'Tipos', 4 from gat_sistema.produtos where ativo = 'sim' and id = ".$idProduto);

            $registros = Configuracao::where('idProduto', '=', $idProduto)->get();
        }

        return view('produtos.configuracoes.index',compact('registros', 'msg'));
    }

    public function registros($id, Request $msg)
    {
        $msg = $msg->input('msg');

        $configuracoes = Configuracao::find($id);

        $registros = Registro::where('idConfig', '=', $id)->get();

        return view('produtos.configuracoes.registros',compact('registros', 'configuracoes', 'msg'));
    }

    public function adicionar($id)
    {
        $configuracoes = Configuracao::find($id);

        $registros = Registro::where('idConfig', '=', $id)->get();

        return view('produtos.configuracoes.adicionar', compact('registros', 'configuracoes'));
    }

    public function salvar($id, Request $req)
    {
        $msg = 'ok';

        $configuracoes = Configuracao::find($id);

        $registros = Registro::where('idConfig', '=', $id)->get();

        $dados = $req->all();
        Registro::create($dados);

        return redirect()->route('produtos.configuracoes.registros', $id);
    }

    public function editar($id)
    {
        $registro = Registro::find($id);

        $configuracoes = Configuracao::find($registro['idConfig']);

        return view('produtos.configuracoes.editar', compact('registro', 'configuracoes'));
    }

    public function atualizar(Request $req, $id)
    {
        $registro = Registro::find($id);

        $configuracoes = Configuracao::find($registro['idConfig']);

        $dados = $req->all();

        Registro::find($id)->update($dados);

        return redirect()->route('produtos.configuracoes.registros', $registro['idConfig']);
    }

    public function excluir($id)
    {
        $registro = Registro::find($id);

        $configuracoes = Configuracao::find($registro['idConfig']);

        $dado = Registro::find($id);
        $dado->delete();

        return redirect()->route('produtos.configuracoes.registros', $registro['idConfig']);
    }

}