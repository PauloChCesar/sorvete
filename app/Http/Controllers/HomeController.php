<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use DB;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        Session::set('SessionIdUS', Auth::id());

        if (Session::get('SessionIdUS') != null){

            $registros = DB::select("select p.varSession FROM usuariospermissoes up INNER JOIN sispermissoes p ON p.id = up.idPer WHERE idUS = 1 AND modulo = 'nao'", [1]);

            foreach ($registros as $codigo) {
                Session::set('perm'.$codigo->varSession, true);
            }

            $redirect = "home";
        }else{
            Session::set('permVenAdm', false);
            Session::set('permUsuPerm', false);
            Session::set('permUsuList', false);
            Session::set('permUsuAdmin', false);
            Session::set('permTicAdm', false);
            Session::set('permSobAdm', false);
            Session::set('permSisDash', false);
            Session::set('permRegAdm', false);
            Session::set('permProdAdm', false);
            Session::set('permTickAdm', false);
            Session::set('permMarcAdm', false);
            Session::set('permCtgAdm', false);
            Session::set('permDuvAdm', false);
            Session::set('permCliAdm', false);
            Session::set('permAnaAdm', false);

            $redirect = "auth/login";
        }
        return view($redirect);
    }
}
