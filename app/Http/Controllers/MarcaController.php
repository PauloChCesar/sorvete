<?php

namespace App\Http\Controllers;

use App\Marca;
use Illuminate\Http\Request;

class MarcaController extends Controller
{
    public function index(Request $msg)
    {
        $msg = $msg->input('msg');

        $registros = Marca::all();

        return view('marcas.index',compact('registros', 'msg'));
    }

    public function adicionar()
    {
        return view('marcas.adicionar');
    }

    public function salvar(Request $req)
    {
        $dados = $req->all();

        Marca::create($dados);

        return redirect()->route('marcas', 'msg=ok');
    }



    public function editar($id)
    {
        $registro = Marca::find($id);

        return view('marcas.editar', compact('registro'));
    }

    public function atualizar(Request $req, $id)
    {
        $dados = $req->all();

        Marca::find($id)->update($dados);

        return redirect()->route('marcas', 'msg=ok');
    }



    public function excluir($id)
    {
        $dado = Marca::find($id);
        $dado->delete();

        return redirect()->route('marcas', 'msg=del');
    }
}
