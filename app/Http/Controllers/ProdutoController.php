<?php

namespace App\Http\Controllers;

use App\Produto;
use Illuminate\Http\Request;
use App\Categoria;
use App\Marca;
use App\ProdutoImagem;
use DB;
use App\Quotation;

class ProdutoController extends Controller
{
    public function index(Request $msg)
    {
        $msg = $msg->input('msg');

        $idTicket = app('request')->input('idTicket');

        $registros = Produto::all();

        return view('produtos.index',compact('registros', 'msg', 'idTicket'));
    }

    public function adicionar(Request $msg)
    {
        $categorias = Categoria::all();
        $marcas = Marca::all();

        return view('produtos.adicionar', compact('categorias', 'marcas'));
    }

    public function salvar(Request $req)
    {
        $dados = $req->all();

        if(isset($dados['promocao']))
        {
            $dados['promocao'] = 'sim';
        }else{
            $dados['promocao'] = 'nao';
        }

        if(isset($dados['porPeso']))
        {
            $dados['porPeso'] = 'sim';
        }else{
            $dados['porPeso'] = 'nao';
        }

        Produto::create($dados);

        return redirect()->route('produtos', 'msg=ok');
    }



    public function editar($id)
    {
        $registro = Produto::find($id);
        $categorias = Categoria::all();
        $marcas = Marca::all();

        return view('produtos.editar', compact('registro', 'categorias', 'marcas'));
    }

    public function atualizar(Request $req, $id)
    {
        $dados = $req->all();

        if(isset($dados['promocao']))
        {
            $dados['promocao'] = 'sim';
        }else{
            $dados['promocao'] = 'nao';
        }

        if(isset($dados['porPeso']))
        {
            $dados['porPeso'] = 'sim';
        }else{
            $dados['porPeso'] = 'nao';
        }

        Produto::find($id)->update($dados);

        return redirect()->route('produtos', 'msg=ok');
    }



    public function excluir($id)
    {
        $dado = Produto::find($id);
        $dado->delete();

        return redirect()->route('produtos', 'msg=del');
    }

    public function indexImagem($id, Request $msg)
    {
        $msg = $msg->input('msg');

        $registros = ProdutoImagem::where('idProduto', '=', $id)->get();

        return view('produtos.imagens.index',compact('id', 'registros', 'msg'));
    }

    public function adicionarImagem($id)
    {
        $registros = ProdutoImagem::where('idProduto', '=', $id)->get();

        return view('produtos.imagens.adicionar', compact('id', 'registros'));
    }

    public function salvarImagem($id, Request $req)
    {
        $msg = 'ok';

        $registros = ProdutoImagem::where('idProduto', '=', $id)->get();

        $dados = $req->all();

        if($req->hasfile('imagem'))
        {
            $imagem = $req->file('imagem');
            $num = rand(1111,9999);
            $dir = "images";
            $ex = $imagem->guessClientExtension();
            $nomeImagem = "imagem_".$num.".".$ex;

            $imagem->move($dir, $nomeImagem);
            $dados['imagem'] = $dir."/".$nomeImagem;
        }

        ProdutoImagem::create($dados);

        return redirect()->route('produtos.imagens',compact('id', 'registros', 'msg'));
    }

    public function excluirImagem($id, $idImagem)
    {
        $dado = ProdutoImagem::find($idImagem);
        $dado->delete();

        $registros = ProdutoImagem::where('idProduto', '=', $id)->get();

        return redirect()->route('produtos.imagens',compact('id', 'idProduto', 'registros', 'msg'));
    }
}
