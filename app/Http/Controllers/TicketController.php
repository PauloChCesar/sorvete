<?php

namespace App\Http\Controllers;

use App\Ticket;
use Illuminate\Http\Request;
use App\TicketProduto;
use App\TicketMarca;
use App\TicketCategoria;
use DB;
use App\Quotation;

class TicketController extends Controller
{
    public function index(Request $msg)
    {
        $msg = $msg->input('msg');

        $registros = Ticket::all();

        return view('tickets.index',compact('registros', 'msg'));
    }

    public function adicionar()
    {
        return view('tickets.adicionar');
    }

    public function salvar(Request $req)
    {
        $dados = $req->all();

        Ticket::create($dados);

        return redirect()->route('tickets', 'msg=ok');
    }

    public function editar($id)
    {
        $registro = Ticket::find($id);

        return view('tickets.editar', compact('registro'));
    }

    public function atualizar(Request $req, $id)
    {
        $dados = $req->all();

        Ticket::find($id)->update($dados);

        return redirect()->route('tickets', 'msg=ok');
    }

    public function excluir($id)
    {
        $dado = Ticket::find($id);
        $dado->delete();

        return redirect()->route('tickets', 'msg=del');
    }

    //################## Produtos ####################
    public function indexProdutos($id, Request $msg)
    {
        $msg = $msg->input('msg');

        $ticket = Ticket::find($id);
        $registros = DB::table('ticket_produtos')
            ->join('produtos', 'produtos.id', '=', 'ticket_produtos.idProduto')
            ->select('ticket_produtos.*', 'produtos.codigo', 'produtos.titulo')
            ->where('idTicket', '=', $id)
            ->get();

        return view('tickets.produtos.index',compact('id', 'registros', 'ticket', 'msg'));
    }

    public function salvarProdutos($id, $idProduto, Request $req)
    {
        $msg = 'ok';

        $dados['idTicket'] = $id;
        $dados['idProduto'] = $idProduto;

        TicketProduto::create($dados);

        return redirect()->route('tickets.produtos', compact('id', 'msg'));
    }

    public function excluirProdutos($id)
    {
        $msg = 'del';

        $dado = TicketProduto::find($id);
        $id = $dado['idTicket'];
        $dado->delete();

        return redirect()->route('tickets.produtos', compact('id' ,'msg'));
    }
}
