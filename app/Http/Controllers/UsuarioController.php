<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\usuarioPermissoes;
use App\permissoes;
use DB;
use Session;
use App\Quotation;

class UsuarioController extends Controller
{
    public function index(Request $msg)
    {
        $msg = $msg->input('msg');

        $registros = User::all();

        return view('usuarios.index',compact('registros', 'msg'));
    }

    public function adicionar()
    {
        return view('usuarios.adicionar');
    }

    public function salvar(Request $req)
    {
        $dados = $req->all();

        if(isset($dados['administrador']))
        {
            $dados['administrador'] = 'sim';
        }else{
            $dados['administrador'] = 'nao';
        }

        User::create($dados);

        return redirect()->route('usuarios');
    }



    public function editar($id)
    {
        $registro = User::find($id);

        return view('usuarios.editar', compact('registro'));
    }

    public function atualizar(Request $req, $id)
    {
        $dados = $req->all();

        User::find($id)->update($dados);

        return redirect()->route('usuarios');
    }



    public function excluir($id)
    {
        $usuarios = \DB::table('users')->where('id', '>', 0)
                ->get();

        $qtUsuario = count($usuarios);

        if ($qtUsuario > 1)
        {
           $dado = User::find($id);

            $dado->delete();
            return redirect()->route('usuarios', 'msg=del');
        }else{
            return redirect()->route('usuarios', 'msg=ult');
        }
    }
    
    public function permissoes($id, Request $msg)
    {
        $msg = $msg->input('msg');

        $registros = DB::select('select pm.descricao as descPermMod, '.$id.' as idUS, pm.id as idPerMod, CONCAT(COUNT(up.id), "/", count(p.id)) AS qtdeAlt 
            from sispermissoesmodulo as pm 
            inner join sispermissoes as p on pm.id = p.idPerMod and modulo = "nao"
            LEFT JOIN usuariospermissoes up ON up.idPer = p.id
            group by pm.descricao, pm.id', [1]);

        return view('usuarios.permissoes.index',compact('registros', 'msg'));
    }

    public function permissoesEditar($id, $idPerMod)
    {

        $registros = DB::table('sispermissoes as p')
            ->select('p.*', DB::raw('(SELECT count(up.id) from usuariospermissoes up WHERE up.idUS = '.$id.' and up.idPer = p.id) AS marcado'))
            ->where('p.idPerMod', '=', $idPerMod)
            ->where('p.modulo', '=', 'nao')
            ->get();

        return view('usuarios.permissoes.editar', compact('id', 'idPerMod', 'registros'));
    }

    public function permissoesAtualizar(Request $req, $id, $idPerMod)
    {
        $permissoes = $req->get("permissoes");

        DB::table('usuariospermissoes')
        ->where('idUS', $id)
        ->where('idPerMod', $idPerMod)
        ->delete();

        if (is_array($permissoes)){
            foreach ($permissoes as $permissao) 
            {
                DB::table('usuariospermissoes')->insert(
                    [
                        'idUS' => $id,
                        'idPerMod' => $idPerMod,
                        'idPer' => $permissao
                    ]
                );
            }
        }

        $registros = DB::select("select p.varSession FROM usuariospermissoes up INNER JOIN sispermissoes p ON p.id = up.idPer WHERE idUS = 1 AND modulo = 'nao'", [1]);

        
        Session::set('permVenAdm', false);
        Session::set('permUsuPerm', false);
        Session::set('permUsuList', false);
        Session::set('permUsuAdmin', false);
        Session::set('permTicAdm', false);
        Session::set('permSobAdm', false);
        Session::set('permSisDash', false);
        Session::set('permRegAdm', false);
        Session::set('permProdAdm', false);
        Session::set('permTickAdm', false);
        Session::set('permMarcAdm', false);
        Session::set('permCtgAdm', false);
        Session::set('permDuvAdm', false);
        Session::set('permCliAdm', false);
        Session::set('permAnaAdm', false);

        foreach ($registros as $codigo) {
            Session::set('perm'.$codigo->varSession, true);
        }

        return redirect()->route('usuarios.permissoes', compact('id'));
    }
}

