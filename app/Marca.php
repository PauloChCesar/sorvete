<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Marca extends Model
{
    protected $fillable = [
        'id', 'titulo', 'ativo', 'dtInclusao', 'dtExclusao'
    ];

    public $timestamps = false;

    protected $dates = ['dtInclusao','dtExclusao'];
}
