<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Produto extends Model
{
    protected $fillable = [
        'id', 'idCategoria', 'idMarca', 'codigo', 'titulo', 'descricao', 'valorReal', 'valorVenda', 'quantidade', 'promocao', 'porPeso', 'ativo', 'dtInclusao', 'dtExclusao'
    ];

    public $timestamps = false;

    protected $dates = ['dtInclusao','dtExclusao'];
}
