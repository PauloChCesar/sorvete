<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Registro extends Model
{
    protected $fillable = [
        'id', 'idConfig', 'titulo', 'dado', 'ativo', 'dtInclusao', 'dtExclusao'
    ];

    public $timestamps = false;

    protected $dates = ['dtInclusao','dtExclusao'];
}
