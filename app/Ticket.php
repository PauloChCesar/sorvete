<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    protected $fillable = [
        'id', 'codigo', 'titulo', 'validoDe', 'validoAte', 'ativo', 'dtInclusao', 'dtExclusao'
    ];

    public $timestamps = false;

    protected $dates = ['validoDe', 'validoAte','dtInclusao','dtExclusao'];
}
