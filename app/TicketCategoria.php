<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TicketCategoria extends Model
{
    protected $fillable = [
        'id', 'idTicket', 'idCategoria', 'ativo', 'dtInclusao', 'dtExclusao'
    ];

    public $timestamps = false;

    protected $dates = ['dtInclusao','dtExclusao'];
}
