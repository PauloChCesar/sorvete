<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TicketMarca extends Model
{
    protected $fillable = [
        'id', 'idTicket', 'idMarca', 'ativo', 'dtInclusao', 'dtExclusao'
    ];

    public $timestamps = false;

    protected $dates = ['dtInclusao','dtExclusao'];
}
