<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TicketProduto extends Model
{
    protected $fillable = [
        'id', 'idTicket', 'idProduto', 'ativo', 'dtInclusao', 'dtExclusao'
    ];

    public $timestamps = false;

    protected $dates = ['dtInclusao','dtExclusao'];
}
