<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'sobrenome', 'idArea', 'idCargo', 'email', 'password', 'nascimento', 'email', 'telCelular', 'telFixo', 'endereco', 'numero', 'complemento', 'bairro', 'cep', 'estado', 'cidade', 'cpf', 'banco', 'agencia', 'conta', 'ativo', 'dtInclusao', 'dtExclusao',
    ];
    
    public $timestamps = false;

    protected $dates = ['nascimento','dtInclusao','dtExclusao'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
}
