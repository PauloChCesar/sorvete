<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Eloquent;

class permissoes extends Model
{
    public $table = "sisPermissoes";

    protected $fillable = [
        'id', 'idPerMod', 'modulo', 'descricao', 'varSession', 'padrao'
    ];

    public $timestamps = false;
}
