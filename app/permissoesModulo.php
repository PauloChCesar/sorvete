<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Eloquent;

class permissoesModulo extends Model
{
    public $table = "sisPermissoesModulo";

    protected $fillable = [
        'id', 'descricao'
    ];

    public $timestamps = false;
}
