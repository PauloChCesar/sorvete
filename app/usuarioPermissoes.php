<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Eloquent;

class usuarioPermissoes extends Model
{
    public $table = "usuariosPermissoes";

    protected $fillable = [
        'id', 'idUS', 'idPer', 'idPerMod', 'dtInclusao', 'dtExclusao'
    ];

    public $timestamps = false;
    
    protected $dates = ['dtInclusao','dtExclusao'];
}
