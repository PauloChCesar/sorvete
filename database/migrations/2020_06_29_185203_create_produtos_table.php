<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProdutosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('produtos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('idCategoria');
            $table->integer('idMarca');
            $table->string('codigo');
            $table->string('titulo');
            $table->text('descricao');
            $table->decimal('valorReal', 11, 2);
            $table->decimal('valorVenda', 11, 2);
            $table->integer('quantidade');
            $table->enum('promocao', ['sim', 'nao'])->default('sim');
            $table->enum('porPeso', ['sim', 'nao'])->default('sim');
            $table->enum('ativo', ['sim', 'nao'])->default('sim');
            $table->dateTime('dtInclusao')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->dateTime('dtExclusao')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('produtos');
    }
}
