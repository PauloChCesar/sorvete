<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProdutoImagemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('produto_imagems', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('idProduto');
            $table->string('titulo');
            $table->string('imagem');
            $table->enum('ativo', ['sim', 'nao'])->default('sim');
            $table->dateTime('dtInclusao')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->dateTime('dtExclusao')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('produto_imagems');
    }
}
