<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTicketMarcasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ticket_marcas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('idTicket');
            $table->integer('idMarca');
            $table->enum('ativo', ['sim', 'nao'])->default('sim');
            $table->dateTime('dtInclusao')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->dateTime('dtExclusao')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ticket_marcas');
    }
}
