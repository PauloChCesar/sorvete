<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsuariosPermissoesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('usuariosPermissoes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('idUS');
            $table->integer('idPer');
            $table->integer('idPerMod');
            $table->dateTime('dtInclusao')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->dateTime('dtExclusao')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('usuariosPermissoes');
    }
}
