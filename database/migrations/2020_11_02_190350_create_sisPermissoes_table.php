<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSisPermissoesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sisPermissoes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('idPerMod');
            $table->enum('modulo', ['sim', 'nao'])->default('nao');
            $table->string('descricao');
            $table->string('varSession');
            $table->enum('padrao', ['sim', 'nao'])->default('nao');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sisPermissoes');
    }
}
