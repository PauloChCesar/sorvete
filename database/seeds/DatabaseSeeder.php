<?php

use Illuminate\Database\Seeder;
use App\user;
use App\permissoes;
use App\permissoesModulo;
use App\usuarioPermissoes;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call('permissoesModuloSeeder');
        $this->call('permissoesSeeder');
        $this->call('UsuariosSeeder');
        $this->call('usuarioPermissoesSeeder');
    }
}

class permissoesModuloSeeder extends Seeder {

    public function run()
    {
        DB::table('sispermissoesmodulo')->delete();

        permissoesModulo::create([
            'id' => 1,
            'descricao' => 'Sistema'
        ]);

        permissoesModulo::create([
            'id' => 2,
            'descricao' => 'Usuários'
        ]);

        permissoesModulo::create([
            'id' => 3,
            'descricao' => 'Clientes'
        ]);

        permissoesModulo::create([
            'id' => 4,
            'descricao' => 'Análise'
        ]);

        permissoesModulo::create([
            'id' => 5,
            'descricao' => 'Dúvida'
        ]);

        permissoesModulo::create([
            'id' => 6,
            'descricao' => 'Produtos'
        ]);

        permissoesModulo::create([
            'id' => 7,
            'descricao' => 'Registros'
        ]);

        permissoesModulo::create([
            'id' => 8,
            'descricao' => 'Sobre'
        ]);

        permissoesModulo::create([
            'id' => 9,
            'descricao' => 'Tickets'
        ]);

        permissoesModulo::create([
            'id' => 10,
            'descricao' => 'Vendas'
        ]);
    }
}

class permissoesSeeder extends Seeder {

    public function run()
    {
        DB::table('sispermissoes')->delete();

        permissoes::create([
            'id' => 1,
            'idPerMod' => 1,
            'modulo' => 'sim',
            'descricao' => 'Módulo: Sistema',
            'varSession' => 'ModSis',
            'padrao' => 'sim'
        ]);

        permissoes::create([
            'id' => 2,
            'idPerMod' => 1,
            'modulo' => 'nao',
            'descricao' => 'Acessar dashboard',
            'varSession' => 'SisDash',
            'padrao' => 'nao'
        ]);

        permissoes::create([
            'id' => 3,
            'idPerMod' => 2,
            'modulo' => 'sim',
            'descricao' => 'Módulo: Usuario',
            'varSession' => 'ModUsu',
            'padrao' => 'sim'
        ]);

        permissoes::create([
            'id' => 4,
            'idPerMod' => 2,
            'modulo' => 'nao',
            'descricao' => 'Administrar permissões',
            'varSession' => 'UsuPerm',
            'padrao' => 'sim'
        ]);

        permissoes::create([
            'id' => 5,
            'idPerMod' => 2,
            'modulo' => 'nao',
            'descricao' => 'Administrar listas auxiliares',
            'varSession' => 'UsuList',
            'padrao' => 'sim'
        ]);

        permissoes::create([
            'id' => 6,
            'idPerMod' => 2,
            'modulo' => 'nao',
            'descricao' => 'Administrar usuários',
            'varSession' => 'UsuAdmin',
            'padrao' => 'sim'
        ]);

        permissoes::create([
            'id' => 7,
            'idPerMod' => 3,
            'modulo' => 'sim',
            'descricao' => 'Módulo: Clientes',
            'varSession' => 'ModCli',
            'padrao' => 'sim'
        ]);

        permissoes::create([
            'id' => 8,
            'idPerMod' => 3,
            'modulo' => 'nao',
            'descricao' => 'Administrar clientes',
            'varSession' => 'CliAdm',
            'padrao' => 'nao'
        ]);

        permissoes::create([
            'id' => 9,
            'idPerMod' => 4,
            'modulo' => 'sim',
            'descricao' => 'Módulo: Análises',
            'varSession' => 'ModAna',
            'padrao' => 'sim'
        ]);

        permissoes::create([
            'id' => 10,
            'idPerMod' => 4,
            'modulo' => 'nao',
            'descricao' => 'Administrar análises',
            'varSession' => 'AnaAdm',
            'padrao' => 'nao'
        ]);

        permissoes::create([
            'id' => 11,
            'idPerMod' => 5,
            'modulo' => 'sim',
            'descricao' => 'Módulo: Dúvidas',
            'varSession' => 'ModDuv',
            'padrao' => 'sim'
        ]);

        permissoes::create([
            'id' => 12,
            'idPerMod' => 5,
            'modulo' => 'nao',
            'descricao' => 'Administrar dúvidas',
            'varSession' => 'DuvAdm',
            'padrao' => 'nao'
        ]);

        permissoes::create([
            'id' => 13,
            'idPerMod' => 6,
            'modulo' => 'sim',
            'descricao' => 'Módulo: Produtos',
            'varSession' => 'ModProd',
            'padrao' => 'sim'
        ]);

        permissoes::create([
            'id' => 14,
            'idPerMod' => 6,
            'modulo' => 'nao',
            'descricao' => 'Administrar produtos',
            'varSession' => 'ProdAdm',
            'padrao' => 'nao'
        ]);

        permissoes::create([
            'id' => 15,
            'idPerMod' => 7,
            'modulo' => 'sim',
            'descricao' => 'Módulo: Registros',
            'varSession' => 'ModReg',
            'padrao' => 'sim'
        ]);

        permissoes::create([
            'id' => 16,
            'idPerMod' => 7,
            'modulo' => 'nao',
            'descricao' => 'Administrar registros',
            'varSession' => 'RegAdm',
            'padrao' => 'nao'
        ]);

        permissoes::create([
            'id' => 17,
            'idPerMod' => 8,
            'modulo' => 'sim',
            'descricao' => 'Módulo: Sobre',
            'varSession' => 'ModSob',
            'padrao' => 'sim'
        ]);

        permissoes::create([
            'id' => 18,
            'idPerMod' => 8,
            'modulo' => 'nao',
            'descricao' => 'Administrar sobre',
            'varSession' => 'SobAdm',
            'padrao' => 'nao'
        ]);

        permissoes::create([
            'id' => 19,
            'idPerMod' => 9,
            'modulo' => 'sim',
            'descricao' => 'Módulo: Tickets',
            'varSession' => 'ModTic',
            'padrao' => 'sim'
        ]);

        permissoes::create([
            'id' => 20,
            'idPerMod' => 9,
            'modulo' => 'nao',
            'descricao' => 'Administrar tickets',
            'varSession' => 'TicAdm',
            'padrao' => 'nao'
        ]);

        permissoes::create([
            'id' => 21,
            'idPerMod' => 10,
            'modulo' => 'sim',
            'descricao' => 'Módulo: Vendas',
            'varSession' => 'VenTic',
            'padrao' => 'sim'
        ]);

        permissoes::create([
            'id' => 22,
            'idPerMod' => 10,
            'modulo' => 'nao',
            'descricao' => 'Administrar vendas',
            'varSession' => 'VenAdm',
            'padrao' => 'nao'
        ]);

        permissoes::create([
            'id' => 23,
            'idPerMod' => 6,
            'modulo' => 'nao',
            'descricao' => 'Administrar tickets',
            'varSession' => 'TickAdm',
            'padrao' => 'nao'
        ]);

        permissoes::create([
            'id' => 24,
            'idPerMod' => 6,
            'modulo' => 'nao',
            'descricao' => 'Administrar marcas',
            'varSession' => 'MarcAdm',
            'padrao' => 'nao'
        ]);

        permissoes::create([
            'id' => 25,
            'idPerMod' => 6,
            'modulo' => 'nao',
            'descricao' => 'Administrar categorias',
            'varSession' => 'CtgAdm',
            'padrao' => 'nao'
        ]);
    }
}

class UsuariosSeeder extends Seeder {

    public function run()
    {
        DB::table('users')->delete();

        user::create([
            'id' => 1,
            'name' => 'Suporte',
            'sobrenome' => 'Goat',
            'email' => 'paulo.charamba@gmail.com',
            'password' => 'goat12',
            'ativo' => 'sim'
        ]);
    }

}

class usuarioPermissoesSeeder extends Seeder {

    public function run()
    {
        DB::table('usuariosPermissoes')->delete();

        usuarioPermissoes::create([
            'idUS' => 1,
            'idPer' => 1,
            'idPerMod' => 1
        ]);
    }

}
