<div class="form-group row">
	<div class="col-9">
		<label for="titulo"><b>Titulo:</b></label>
		<input class="form-control" type="text" name="titulo" id="titulo" value="{{isset($registro->titulo) ? $registro->titulo : ''}}">
	</div>
	<div class="col-3">
		<label for="titulo"><b>Avaliação:</b></label>
		<div style="margin-top: -5px;">
			<input type="radio" id="star1" name="valor" value="1"{{isset($registro->valor) ? $registro->valor == 1 ? 'checked' : '' : ''}}>
			<label for="star1" class="star_icon" style="font-size: 30px;">★</label>
			<input type="radio" id="star2" name="valor" value="2" {{isset($registro->valor) ? $registro->valor == 2 ? 'checked' : '' : ''}}>
			<label for="star2" class="star_icon" style="font-size: 30px;">★</label>
			<input type="radio" id="star3" name="valor" value="3"{{isset($registro->valor) ? $registro->valor == 3 ? 'checked' : '' : ''}}>
			<label for="star3" class="star_icon" style="font-size: 30px;">★</label>
			<input type="radio" id="star4" name="valor" value="4"{{isset($registro->valor) ? $registro->valor == 4 ? 'checked' : '' : ''}}>
			<label for="star4" class="star_icon" style="font-size: 30px;">★</label>
			<input type="radio" id="star5" name="valor" value="5"{{isset($registro->valor) ? $registro->valor == 5 ? 'checked' : '' : ''}}>
			<label for="star5" class="star_icon" style="font-size: 30px;">★</label>
		</div>
	</div>
</div>

<div class="form-group row">
	<div class="col-6">
		<label for="idCliente"><b>Cliente:</b></label>
		<select class="form-control" id="idCliente" name="idCliente">
			<option value="">-Selecione um cliente-</option>
            @foreach($clientes as $i=>$cliente)
				<option value="{{$cliente->id}}" {{isset($registro->idCliente) ? $registro->idCliente == $cliente->id ? 'selected="selected"' : '' : ''}}>{{$cliente->nome.' '.$cliente->sobrenome}}</option>
            @endforeach
		</select>
	</div>
	<div class="col-6">
		<label for="idProduto"><b>Produto:</b></label>
		<input type="hidden" name="idProduto" value="{{$produto->id}}">
		<select class="form-control" id="idProduto" name="idProduto" disabled>
			<option selected>{{$produto->titulo}}</option>
		</select>
	</div>
</div>

<div class="form-group row">
	<div class="col-12">
		<label for="descricao"><b>Texto:</b></label>
    	<textarea class="form-control" id="descricao" name="descricao" rows="3">{{isset($registro->descricao) ? $registro->descricao : ''}}</textarea>
	</div>
</div>