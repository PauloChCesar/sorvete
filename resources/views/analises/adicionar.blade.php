@section('logado', true)

@extends('layout.admin.site')

@section('titulo','Admin - Dashboard')

@section('conteudo')
<main>
    <div class="container-fluid">
        <h1 class="mt-4">Analises</h1>
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item"><a href="{{route('admin')}}">Dashboard</a></li>
            <li class="breadcrumb-item"><a href="{{route('admin.produtos')}}">Produtos</a></li>
            <li class="breadcrumb-item"><a href="{{route('admin.analises', $idProduto)}}">Analises</a></li>
            <li class="breadcrumb-item active">Adicionar</li>
        </ol>
        <div class="card mb-4">
            <div class="card-body">
                <form action="{{route('admin.analises.salvar')}}" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    @include('admin.analises._form')
                    <button class="btn btn-success">Salvar</button>
                    <button class="btn btn-danger" type="Reset">Resetar</button>
                </form>
            </div>
        </div>
    </div>
</main>
@endsection