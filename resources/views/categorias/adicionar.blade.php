@section('logado', true)

@extends('layouts.site')

@section('titulo','Admin - Dashboard')

@section('conteudo')
<main>
    <div class="container-fluid">
        <h1 class="mt-4">Categorias</h1>
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item"><a href="{{route('home')}}">Dashboard</a></li>
            <li class="breadcrumb-item"><a href="{{route('categorias')}}">Categorias</a></li>
            <li class="breadcrumb-item active">Adicionar</li>
        </ol>
        <div class="card mb-4">
            <div class="card-body">
                <form action="{{route('categorias.salvar')}}" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    @include('categorias._form')
                    <button class="btn btn-success">Salvar</button>
                    <button class="btn btn-danger" type="Reset">Resetar</button>
                </form>
            </div>
        </div>
    </div>
</main>
@endsection