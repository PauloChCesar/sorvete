<div class="form-group row">
	<div class="col-6">
		<label for="nome" class="col-12 col-form-label"><b>Nome:</b></label>
		<input class="form-control" type="text" name="nome" id="nome" value="{{isset($registro->nome) ? $registro->nome : ''}}">
	</div>
	<div class="col-6">
		<label for="sobrenome" class="col-12 col-form-label"><b>Sobrenome:</b></label>
		<input class="form-control" type="text" name="sobrenome" id="sobrenome" value="{{isset($registro->sobrenome) ? $registro->sobrenome : ''}}">
	</div>
</div>

<div class="form-group row">
	<div class="col-8">
		<label for="endereco" class="col-12 col-form-label"><b>Endereço:</b></label>
		<input class="form-control" type="text" name="endereco" id="endereco" value="{{isset($registro->endereco) ? $registro->endereco : ''}}">
	</div>
	<div class="col-2">
		<label for="telefone" class="col-12 col-form-label"><b>Telefone:</b></label>
		<input class="form-control phone_with_ddd" type="tel" name="telefone" id="telefone" value="{{isset($registro->telefone) ? $registro->telefone : '(00) 90000-0000'}}">
	</div>
	<div class="col-2">
		<label for="nascimento" class="col-12 col-form-label"><b>Nascimento:</b></label>
		<input class="form-control" type="date" name="nascimento" id="nascimento" value="{{isset($registro->nascimento) ? $registro->nascimento->format('Y-m-d') : ''}}">
	</div>
</div>

<div class="form-group row">
	<div class="col-4">
		<label for="cidade" class="col-12 col-form-label"><b>Cidade:</b></label>
		<input class="form-control" type="text" name="cidade" id="cidade" value="{{isset($registro->cidade) ? $registro->cidade : ''}}">
	</div>
	<div class="col-4">
		<label for="bairro" class="col-12 col-form-label"><b>Bairro:</b></label>
		<input class="form-control" type="text" name="bairro" id="bairro" value="{{isset($registro->bairro) ? $registro->bairro : ''}}">
	</div>
	<div class="col-2">
		<label for="cep" class="col-12 col-form-label"><b>CEP:</b></label>
		<input class="form-control cep" type="text" name="cep" id="cep" value="{{isset($registro->cep) ? $registro->cep : ''}}">
	</div>
	<div class="col-2">
		<label for="pais" class="col-12 col-form-label"><b>País:</b></label>
		<input class="form-control" type="text" name="pais" id="pais" value="{{isset($registro->pais) ? $registro->pais : ''}}">
	</div>
</div>

<div class="form-group row">
	<div class="col-6">
		<label for="complemento" class="col-12 col-form-label"><b>Complemento:</b></label>
		<input class="form-control" type="text" name="complemento" id="complemento" value="{{isset($registro->complemento) ? $registro->complemento : ''}}">
	</div>
</div>

<div class="form-group row">
	<div class="col-6">
		<label for="email" class="col-12 col-form-label"><b>E-mail:</b></label>
		<input class="form-control" type="email" name="email" id="email" aria-describedby="emailHelp" value="{{isset($registro->email) ? $registro->email : ''}}">
	</div>
	<div class="col-6">
		<label for="senha" class="col-12 col-form-label"><b>Senha:</b></label>
		<input class="form-control" type="password" name="senha" id="senha" value="{{isset($registro->senha) ? $registro->senha : ''}}">
	</div>
</div>