<div class="form-group row">
	<div class="col-6">
		<label for="idCliente"><b>Cliente:</b></label>
		<select class="form-control" id="idCliente" name="idCliente">
			<option value="">-Selecione um cliente-</option>
            @foreach($clientes as $i=>$cliente)
				<option value="{{$cliente->id}}" {{isset($registro->idCliente) ? $registro->idCliente == $cliente->id ? 'selected="selected"' : '' : ''}}>{{$cliente->nome.' '.$cliente->sobrenome}}</option>
            @endforeach
		</select>
	</div>
</div>

<div class="form-group row">
	<div class="col-12">
		<label for="texto"><b>Depoimento:</b></label>
    	<textarea class="form-control" id="texto" name="texto" rows="3">{{isset($registro->texto) ? $registro->texto : ''}}</textarea>
	</div>
</div>