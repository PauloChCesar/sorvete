@section('logado', true)

@extends('layout.admin.site')

@section('titulo','Admin - Dashboard')

@section('conteudo')
<main>
    <div class="container-fluid">
        @include('layout.admin._includes.menssagem')
        <h1 class="mt-4">Depoimentos</h1>
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item"><a href="{{route('admin')}}">Dashboard</a></li>
            <li class="breadcrumb-item active">Depoimentos</li>
        </ol>
        <div class="card mb-4">
            <div class="card-body">
                <div style="margin-bottom: 10px;">
                    <a href="{{route('admin.depoimentos.adicionar')}}" title="Adicionar"><i class="fas fa-plus fa-2x text-success"></i></a>
                </div>
                <table class="table table-bordered" width="100%" cellspacing="0">
                    <thead class="thead-dark">
                        <tr>
                            <th scope="col" width="20">#</th>
                            <th scope="col" width="50">&nbsp;</th>
                            <th scope="col">Cliente</th>
                            <th scope="col" width="400">Resumo</th>
                            <th scope="col" width="100">Inclusão</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($registros as $i=>$registro)
                            <tr>
                                <td scope="row">{{++$i}}</td>
                                <td class="bg-dark">
                                    <div class="btn-group dropright">
                                        <button class="btn btn-link btn-sm order-1 order-lg-0" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-bars"></i></button>
                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton" style="width: 110px;">
                                            <a href="{{route('admin.depoimentos.editar',$registro->id)}}" style="display: inline; margin-left: 20px; margin-right: 10px;" title="Editar"><i class="fas fa-edit text-success"></i></a>
                                            <a href="{{route('admin.depoimentos.excluir',$registro->id)}}" style="display: inline; margin-left: 20px; margin-right: 10px;" title="Excluir"><i class="fas fa-trash text-danger"></i></a>
                                        </div>
                                    </div>
                                </td>
                                <td>{{$registro->nome.' '.$registro->sobrenome}}</td>
                                <td>{{Str::limit($registro->texto, 50)}}</td>
                                <td>{{date('d/m/Y', strtotime($registro->dtInclusao))}}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</main>
@endsection