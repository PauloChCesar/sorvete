@section('logado', true)

@extends('layout.admin.site')

@section('titulo','Admin - Dashboard')

@section('conteudo')
<main>
    <div class="container-fluid">
        <h1 class="mt-4">Usuarios</h1>
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item"><a href="{{route('admin')}}">Dashboard</a></li>
            <li class="breadcrumb-item"><a href="{{route('admin.usuarios')}}">Usuarios</a></li>
            <li class="breadcrumb-item active">Adicionar</li>
        </ol>
        <div class="card mb-4">
            <div class="card-body">
                <form action="{{route('admin.usuarios.salvar')}}" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    @include('admin.usuarios._form')
                    <button class="btn btn-success">Salvar</button>
                    <button class="btn btn-danger" type="Reset">Resetar</button>
                </form>
            </div>
        </div>
    </div>
</main>
@endsection