		@if((string)$msg == 'ok')
			<div class="alert alert-success alert-dismissible fade show card mb-4 flutuar" role="alert" style="margin-top: 5px;">
			    <strong>Ação realizada com sucesso!</strong>
			    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
			        <span aria-hidden="true">&times;</span>
			    </button>
			</div>
		@elseif ($msg == 'adm')
			<div class="alert alert-warning alert-dismissible fade show card mb-4 flutuar" role="alert" style="margin-top: 5px;">
			    <strong>Este usuário é administrador!</strong>
			    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
			        <span aria-hidden="true">&times;</span>
			    </button>
			</div>
		@elseif ($msg == 'del')
			<div class="alert alert-danger alert-dismissible fade show card mb-4 flutuar" role="alert" style="margin-top: 5px;">
			    <strong>Registro excluído com sucesso!</strong>
			    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
			        <span aria-hidden="true">&times;</span>
			    </button>
			</div>
		@elseif ($msg == 'ult')
			<div class="alert alert-info alert-dismissible fade show card mb-4 flutuar" role="alert" style="margin-top: 5px;">
			    <strong>Não é possivel realizar esta ação!</strong>&nbsp;É necessário ao menos 1 registro na tabela.
			    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
			        <span aria-hidden="true">&times;</span>
			    </button>
			</div>
		@endif
