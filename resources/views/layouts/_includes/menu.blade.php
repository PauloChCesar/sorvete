<nav class="sb-topnav navbar navbar-expand navbar-dark bg-dark">
    <a class="navbar-brand" href="/home">Área restrita</a>
    <button class="btn btn-link btn-sm order-1 order-lg-0" id="sidebarToggle" href="#"><i class="fas fa-bars"></i></button>
    <!-- Navbar Search-->
    <div class="d-none d-md-inline-block form-inline ml-auto mr-0 mr-md-3 my-2 my-md-0"></div>
    <!-- Navbar-->
    <ul class="navbar-nav ml-auto ml-md-0">
        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" id="userDropdown" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-user fa-fw"></i></a>
            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
                <a class="dropdown-item" href="{{route('usuarios.editar', Session::get('SessionIdUS'))}}">Cadastro</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="{{ url('/logout') }}"
                    onclick="event.preventDefault();
                             document.getElementById('logout-form').submit();">
                    Logout
                </a>

                <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>

            </div>
        </li>
    </ul>
</nav>
<div id="layoutSidenav">
    <div id="layoutSidenav_nav">
        <nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
            <div class="sb-sidenav-menu">
                <div class="nav">
                    <div class="sb-sidenav-menu-heading">Core</div>
                    <a class="nav-link" href="{{route('home')}}">
                        <div class="sb-nav-link-icon"><i class="fas fa-tachometer-alt"></i></div>
                        Dashboard
                    </a>
                    @if(Session::get('permCliAdm'))
                        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseClientes" aria-expanded="false" aria-controls="collapseClientes">
                            <div class="sb-nav-link-icon"><i class="fas fa-users"></i></div>
                            Clientes
                            <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                        </a>
                        <div class="collapse" id="collapseClientes" aria-labelledby="headingOne" data-parent="#sidenavAccordion">
                            <nav class="sb-sidenav-menu-nested nav">
                                <a class="nav-link" href="{{route('clientes')}}">Clientes</a>
                        </div>
                        <div class="collapse" id="collapseClientes" aria-labelledby="headingOne" data-parent="#sidenavAccordion">
                            <nav class="sb-sidenav-menu-nested nav">
                                <a class="nav-link" href="{{route('depoimentos')}}">Depoimentos</a>
                        </div>
                    @endif
                    @if(Session::get('permVenAdm'))
                        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseVendas" aria-expanded="false" aria-controls="collapseVendas">
                            <div class="sb-nav-link-icon"><i class="fas fa-shopping-cart"></i></div>
                            Vendas
                            <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                        </a>
                        <div class="collapse" id="collapseVendas" aria-labelledby="headingOne" data-parent="#sidenavAccordion">
                            <nav class="sb-sidenav-menu-nested nav">
                                <a class="nav-link" href="{{route('vendas')}}">Vendas</a>
                        </div>
                    @endif

                    <!--
                    <div class="sb-sidenav-menu-heading">E-Commerce</div>
                    <a class="nav-link" href="{{route('home')}}">
                        <div class="sb-nav-link-icon"><i class="fas fa-home"></i></div>
                        Home
                    </a>
                    -->
                    @if(Session::get('permProdAdm'))
                        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseProdutos" aria-expanded="false" aria-controls="collapseProdutos">
                            <div class="sb-nav-link-icon"><i class="fas fa-cubes"></i></div>
                            Produtos
                            <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                        </a>
                        <div class="collapse" id="collapseProdutos" aria-labelledby="headingOne" data-parent="#sidenavAccordion">
                            <nav class="sb-sidenav-menu-nested nav">
                                <a class="nav-link" href="{{route('produtos')}}">Produtos</a>
                        </div>
                        @if(Session::get('permCtgAdm'))
                            <div class="collapse" id="collapseProdutos" aria-labelledby="headingOne" data-parent="#sidenavAccordion">
                                <nav class="sb-sidenav-menu-nested nav">
                                    <a class="nav-link" href="{{route('categorias')}}">Categorias</a>
                            </div>
                        @endif
                        @if(Session::get('permMarcAdm'))
                            <div class="collapse" id="collapseProdutos" aria-labelledby="headingOne" data-parent="#sidenavAccordion">
                                <nav class="sb-sidenav-menu-nested nav">
                                    <a class="nav-link" href="{{route('marcas')}}">Marcas</a>
                            </div>
                        @endif
                        @if(Session::get('permTickAdm'))
                            <div class="collapse" id="collapseProdutos" aria-labelledby="headingOne" data-parent="#sidenavAccordion">
                                <nav class="sb-sidenav-menu-nested nav">
                                    <a class="nav-link" href="{{route('tickets')}}">Tickets</a>
                            </div>
                        @endif
                    @endif
                    @if(Session::get('permSobAdm'))
                        <a class="nav-link" href="charts.html">
                            <div class="sb-nav-link-icon"><i class="fas fa-book-open"></i></div>
                            Sobre nós
                        </a>
                    @endif
                    @if(Session::get('permDuvAdm'))
                        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseDuvidas" aria-expanded="false" aria-controls="collapseDuvidas">
                            <div class="sb-nav-link-icon"><i class="fas fa-flag"></i></div>
                            Duvidas
                            <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                        </a>
                        <div class="collapse" id="collapseDuvidas" aria-labelledby="headingOne" data-parent="#sidenavAccordion">
                            <nav class="sb-sidenav-menu-nested nav">
                                <a class="nav-link" href="layout-static.html">Trocas e Devoluções</a>
                        </div>
                        <div class="collapse" id="collapseDuvidas" aria-labelledby="headingOne" data-parent="#sidenavAccordion">
                            <nav class="sb-sidenav-menu-nested nav">
                                <a class="nav-link" href="layout-static.html">FAQ</a>
                        </div>
                    @endif

                    @if(Session::get('permUsuAdmin'))
                        <div class="sb-sidenav-menu-heading">Gerenciamento</div>
                        <a class="nav-link" href="{{route('usuarios')}}">
                            <div class="sb-nav-link-icon"><i class="fas fa-user"></i></div>
                            Usuários
                        </a>
                    @endif
                </div>
            </div>
            <div class="sb-sidenav-footer">
                <div class="small">Logado como:</div>
                Paulo Cesar
            </div>
        </nav>
    </div>
    <div id="layoutSidenav_content">