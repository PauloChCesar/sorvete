<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>@yield('titulo')</title>
		<link href="<?php echo asset('css/style.css')?>" rel="stylesheet" type="text/css" media="all" />
        <link href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css" rel="stylesheet" crossorigin="anonymous" />
        <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/js/all.min.js" crossorigin="anonymous"></script>
        <script src="https://cdn.tiny.cloud/1/1esvmcru0imfhwje58tuek1kx1t770ipwx8mjofkb70ujesl/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
        <script>
            var demoBaseConfig = {
              selector: "textarea#classic",
              height: 500,
              resize: false,
              autosave_ask_before_unload: false,
              powerpaste_allow_local_images: true,
              plugins: [
                "a11ychecker advcode advlist anchor autolink codesample fullscreen help image imagetools tinydrive",
                " lists link media noneditable powerpaste preview",
                " searchreplace table template tinymcespellchecker visualblocks wordcount"
              ],
              toolbar:
                "insertfile a11ycheck undo redo | bold italic | forecolor backcolor | template codesample | alignleft aligncenter alignright alignjustify | bullist numlist | link image tinydrive",
              content_css: '//www.tiny.cloud/css/codepen.min.css',
              spellchecker_dialog: true,
              spellchecker_whitelist: ['Ephox', 'Moxiecode'],
              tinydrive_demo_files_url: '/docs/demo/tiny-drive-demo/demo_files.json',
              tinydrive_token_provider: function (success, failure) {
                success({ token: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJqb2huZG9lIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.Ks_BdfH4CWilyzLNk8S2gDARFhuxIauLa8PwhdEQhEo' });
              }
            };

            tinymce.init(demoBaseConfig);
        </script>
        <script src="<?php echo asset('js/jscolor.js') ?>"></script>
    </head>
    <body class="sb-nav-fixed">