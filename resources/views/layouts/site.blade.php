@include('layouts._includes.topo')

@hasSection('logado')
	@include('layouts._includes.menu')
@endif

@yield('conteudo')

@hasSection('logado')
	@include('layouts._includes.footer')
@endif

@include('layouts._includes.script')