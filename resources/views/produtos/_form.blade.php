<div class="form-group row">
	<div class="col-2">
		<label for="codigo" class="col-12 col-form-label"><b>Código:</b></label>
		<input class="form-control" type="text" name="codigo" id="codigo" value="{{isset($registro->codigo) ? $registro->codigo : ''}}">
	</div>
	<div class="col-7">
		<label for="titulo" class="col-12 col-form-label"><b>Titulo:</b></label>
		<input class="form-control" type="text" name="titulo" id="titulo" value="{{isset($registro->titulo) ? $registro->titulo : ''}}">
	</div>
	<div class="col-3">
		<label for="idCategoria" class="col-12 col-form-label"><b>Categoria:</b></label>
		<select class="form-control" id="idCategoria" name="idCategoria">
			<option value="">-Selecione uma categoria-</option>
            @foreach($categorias as $i=>$categoria)
				<option value="{{$categoria->id}}" {{isset($registro->idCategoria) ? $registro->idCategoria == $categoria->id ? 'selected="selected"' : '' : ''}}>{{$categoria->titulo}}</option>
            @endforeach
		</select>
	</div>
</div>

<div class="form-group row">
	<div class="col-3">
		<label for="idMarca" class="col-12 col-form-label"><b>Marca:</b></label>
		<select class="form-control" id="idMarca" name="idMarca">
			<option value="">-Selecione uma marca-</option>
            @foreach($marcas as $i=>$marca)
				<option value="{{$marca->id}}" {{isset($registro->idMarca) ? $registro->idMarca == $marca->id ? 'selected="selected"' : '' : ''}}>{{$marca->titulo}}</option>
            @endforeach
		</select>
	</div>
	<div class="col-2">
		<label for="valorReal" class="col-12 col-form-label"><b>Valor real:</b></label>
		<input class="form-control moneyL text-right" type="text" name="valorReal" id="valorReal" value="{{isset($registro->valorReal) ? $registro->valorReal : '0.00'}}" max="99999">
	</div>
	<div class="col-2">
		<label for="valorVenda" class="col-12 col-form-label"><b>Valor de venda:</b></label>
		<input class="form-control moneyL text-right" type="text" name="valorVenda" id="valorVenda" value="{{isset($registro->valorVenda) ? $registro->valorVenda : '0.00'}}">
	</div>
	<div class="col-2">
		<label for="quantidade" class="col-12 col-form-label"><b>Quantidade:</b></label>
		<input class="form-control integer text-right" type="text" name="quantidade" id="quantidade" value="{{isset($registro->quantidade) ? $registro->quantidade : '0'}}">
	</div>
</div>
<div class="form-group row" style="margin-left: 2px;">
	<div class="col-2">
		<label class="col-12 col-form-label" for="promocao"><br><br>
			<input class="form-check-input" type="checkbox" name="promocao" id="promocao" value="true" {{isset($registro->promocao) && $registro->promocao == 'sim' ? 'checked' : ''}}>
			Promoção
		</label>
	</div>
	<div class="col-3">
		<label class="col-12 col-form-label" for="porPeso"><br><br>
			<input class="form-check-input" type="checkbox" name="porPeso" id="porPeso" value="true" {{isset($registro->porPeso) && $registro->porPeso == 'sim' ? 'checked' : ''}}>
			Vendido por peso
		</label>
	</div>
</div>

<div class="form-group row">
	<div class="col-12">
		<label for="descricao"><b>Descricao:</b></label>
    	<textarea class="form-control" id="classic" name="descricao" rows="3">{{isset($registro->descricao) ? $registro->descricao : ''}}</textarea>
	</div>
</div>