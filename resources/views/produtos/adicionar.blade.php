@section('logado', true)

@extends('layouts.site')

@section('titulo','Admin - Dashboard')

@section('conteudo')
<main>
    <div class="container-fluid">
        <h1 class="mt-4">Produtos</h1>
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item"><a href="{{route('home')}}">Dashboard</a></li>
            <li class="breadcrumb-item"><a href="{{route('produtos')}}">Produtos</a></li>
            <li class="breadcrumb-item active">Adicionar</li>
        </ol>
        <div class="card mb-4">
            <div class="card-body">
                <form action="{{route('produtos.salvar')}}" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    @include('produtos._form')
                    <button class="btn btn-success">Salvar</button>
                    <button class="btn btn-danger" type="Reset">Resetar</button>
                </form>
            </div>
        </div>
    </div>
</main>
<script src="https://code.jquery.com/jquery-1.9.1.js"></script>
<script type="text/javascript">
    $('#codigo').on('keypress',function(event){
      //Tecla 13 = Enter
      if(event.which == 13) {
        //cancela a ação padrão
        event.preventDefault();
      }
    });
</script>
@endsection