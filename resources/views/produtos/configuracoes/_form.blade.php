<div class="form-group row">
	<div class="col-10">
		<label for="titulo" class="col-12 col-form-label"><b>Titulo:</b></label>
		<input class="form-control" type="text" name="titulo" id="titulo" value="{{isset($registro->titulo) ? $registro->titulo : ''}}">
	</div>
	@if($configuracoes->tipo == 1)
		<div class="col-2">
			<label for="titulo" class="col-12 col-form-label"><b>Cor:</b></label>
			<button class="btn form-control" data-jscolor="{valueElement:'#val4'}"></button>
	        <input id="val4" value="{{isset($registro->dado) ? $registro->dado : '#0000ff'}}" name="dado" type="hidden">
		</div>
	@elseif($configuracoes->tipo == 2)
		<div class="col-2">
			<label for="dado" class="col-12 col-form-label"><b>Dimensão:</b></label>
			<input class="form-control" type="text" name="dado" id="dado" value="{{isset($registro->dado) ? $registro->dado : ''}}">
		</div>
	@elseif($configuracoes->tipo == 3)
		<div class="col-2">
			<label for="dado" class="col-12 col-form-label"><b>Especificações:</b></label>
			<input class="form-control" type="text" name="dado" id="dado" value="{{isset($registro->dado) ? $registro->dado : ''}}">
		</div>
	@elseif($configuracoes->tipo == 4)
		<div class="col-2">
			<label for="dado" class="col-12 col-form-label"><b>Tipos:</b></label>
			<input class="form-control" type="text" name="dado" id="dado" value="{{isset($registro->dado) ? $registro->dado : ''}}">
		</div>
	@endif
</div>