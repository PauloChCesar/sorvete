@section('logado', true)

@extends('layout.admin.site')

@section('titulo','Admin - Dashboard')

@section('conteudo')
<main>
    <div class="container-fluid">
        <h1 class="mt-4">Configurações</h1>
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item"><a href="{{route('admin')}}">Dashboard</a></li>
            <li class="breadcrumb-item"><a href="{{route('admin.produtos')}}">Produtos</a></li>
            <li class="breadcrumb-item"><a href="{{route('admin.produtos.configuracoes',$configuracoes->idProduto)}}">Configurações</a></li>
            <li class="breadcrumb-item"><a href="{{route('admin.produtos.configuracoes.registros', $configuracoes->id)}}">Registros</a></li>
        </ol>
        <div class="card mb-4">
            <div class="card-body">
                <form action="{{route('admin.produtos.configuracoes.registros.atualizar', $registro->id)}}" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <input type="hidden" name="_method" value="put">
                    @include('admin.produtos.configuracoes._form')
                    <button class="btn btn-success">Salvar</button>
                    <button class="btn btn-danger" type="Reset">Resetar</button>
                </form>
            </div>
        </div>
    </div>
</main>
@endsection