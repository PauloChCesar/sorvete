@section('logado', true)

@extends('layouts.site')

@section('titulo','Admin - Dashboard')

@section('conteudo')
<main>
    <div class="container-fluid">
        @include('layouts._includes.menssagem')
        <h1 class="mt-4">Configurações</h1>
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item"><a href="{{route('home')}}">Dashboard</a></li>
            <li class="breadcrumb-item"><a href="{{route('produtos')}}">Produtos</a></li>
            <li class="breadcrumb-item active">Configurações</li>
        </ol>
        <div class="card mb-4">
            <div class="card-body">
                <table class="table table-bordered" width="100%" cellspacing="0">
                    <thead class="thead-dark">
                        <tr>
                            <th scope="col" width="20">#</th>
                            <th scope="col" width="50">&nbsp;</th>
                            <th scope="col" style="width: auto;">Titulo</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($registros as $i=>$registro)
                            <tr>
                                <td scope="row">{{++$i}}</td>
                                <td class="bg-dark">
                                    <div class="btn-group dropright">
                                        <button class="btn btn-link btn-sm order-1 order-lg-0" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-bars"></i></button>
                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton" style="width: 50px !important;">
                                            <a href="{{route('produtos.configuracoes.registros',$registro->id)}}" style="display: inline; margin-left: 15px; margin-right: 10px;" title="Registros"><i class="fas fa-folder text-warning"></i></a>
                                        </div>
                                    </div>
                                </td>
                                <td>{{$registro->titulo}}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</main>
@endsection