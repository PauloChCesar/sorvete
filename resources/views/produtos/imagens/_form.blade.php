<div class="form-group row">
	<div class="col-6">
		<label for="titulo" class="col-12 col-form-label"><b>Titulo:</b></label>
		<input class="form-control" type="text" name="titulo" id="titulo" value="{{isset($registro->titulo) ? $registro->titulo : ''}}">
	</div>
</div>

<div class="form-group row">
	<div class="col-6">
		<label for="titulo" class="col-12 col-form-label"><b>Imagem:</b></label>
		<div class="custom-file mb-3">
			<input type="file" class="custom-file-input" id="imagem" name="imagem">
			<label class="custom-file-label" for="imagem">Selecione uma imagem...</label>
	    </div>
	</div>
</div>