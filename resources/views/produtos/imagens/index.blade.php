@section('logado', true)

@extends('layouts.site')

@section('titulo','Admin - Dashboard')

@section('conteudo')
<main>
    <div class="container-fluid">
        @include('layouts._includes.menssagem')
        <h1 class="mt-4">Imagens</h1>
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item"><a href="{{route('home')}}">Dashboard</a></li>
            <li class="breadcrumb-item"><a href="{{route('produtos')}}">Produtos</a></li>
            <li class="breadcrumb-item active">Imagens</li>
        </ol>
        <div class="card mb-4">
            <div class="card-body">
                <div style="margin-bottom: 10px;">
                    <a href="{{route('produtos.imagens.adicionar', $id)}}" title="Adicionar"><i class="fas fa-plus fa-2x text-success"></i></a>
                </div>
                <table class="table table-bordered" width="100%" cellspacing="0">
                    <thead class="thead-dark">
                        <tr>
                            <th scope="col" width="20">#</th>
                            <th scope="col" width="50">&nbsp;</th>
                            <th scope="col" style="width: auto;">Titulo</th>
                            <th scope="col" width="100">Inclusão</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($registros as $i=>$registro)
                            <tr>
                                <td scope="row">{{++$i}}</td>
                                <td class="bg-dark">
                                    <div class="btn-group dropright">
                                        <button class="btn btn-link btn-sm order-1 order-lg-0" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-bars"></i></button>
                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton" style="width: 105px !important;">   
                                            <a data-toggle="modal" data-target="{{'#exampleModal'.$i}}" style="display: inline; margin-left: 15px; margin-right: 10px;" title="Registros"><i class="fas fa-eye text-primary"></i></a>
                                            <a href="{{route('produtos.imagens.excluir', ['id'=>$id,'idImagem'=>$registro->id])}}" style="display: inline; margin-left: 15px; margin-right: 10px;" title="Excluir"><i class="fas fa-trash text-danger"></i></a>
                                        </div>
                                    </div>
                                </td>
                                <td>{{$registro->titulo}}</td>
                                <td>{{$registro->dtInclusao->format('d/m/Y')}}</td>
                            </tr>
                            <!-- Modal -->
                            <div class="modal fade " id="{{'exampleModal'.$i}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                              <div class="modal-dialog modal-sm" role="document">
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalCenterTitle">{{$registro->titulo}}</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                      <span aria-hidden="true">&times;</span>
                                    </button>
                                  </div>
                                  <div class="modal-body">
                                    <img src="{{asset($registro->imagem)}}" style="width: 100%;">
                                  </div>
                                  <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                                  </div>
                                </div>
                              </div>
                            </div>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</main>
@endsection