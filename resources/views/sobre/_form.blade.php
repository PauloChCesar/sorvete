<div class="form-group row">
	<div class="col-10">
		<label for="nome" class="col-2 col-form-label"><b>Nome:</b></label>
		<input class="form-control" type="text" name="nome" id="nome" value="{{isset($registro->nome) ? $registro->nome : ''}}">
	</div>
	<div class="col-2">
		<label for="nascimento" class="col-12 col-form-label"><b>Nascimento:</b></label>
		<input class="form-control" type="date" name="nascimento" id="nascimento" value="{{isset($registro->nascimento) ? $registro->nascimento->format('Y-m-d') : ''}}">
	</div>
</div>

<div class="form-group row">
	<div class="col-6">
		<label for="email" class="col-2 col-form-label"><b>E-mail:</b></label>
		<input class="form-control" type="email" name="email" id="email" aria-describedby="emailHelp" value="{{isset($registro->email) ? $registro->email : ''}}">
	</div>
	<div class="col-6">
		<label for="password" class="col-2 col-form-label"><b>Senha:</b></label>
		<input class="form-control" type="password" name="password" id="password" aria-describedby="emailHelp" value="{{isset($registro->password) ? $registro->password : ''}}">
	</div>
</div>

<div class="form-group row form-check">
	<div class="col-12">
		<input class="form-check-input" type="checkbox" name="administrador"  value="true" {{isset($registro->administrador) && $registro->administrador == 'sim' ? 'checked' : ''}}>
		<label class="form-check-label" for="administrador">Administrador</label>
	</div>
</div>