<div class="form-group row">
	<div class="col-9">
		<label for="titulo" class="col-12 col-form-label"><b>Titulo:</b></label>
		<input class="form-control" type="text" name="titulo" id="titulo" value="{{isset($registro->titulo) ? $registro->titulo : ''}}">
	</div>
	<div class="col-3">
		<label for="codigo" class="col-12 col-form-label"><b>Código:</b></label>
		<input class="form-control" type="text" name="codigo" id="codigo" value="{{isset($registro->codigo) ? $registro->codigo : ''}}">
	</div>
</div>

<div class="form-group row">
	<div class="col-5">
		<label for="codigo" class="col-12 col-form-label"><b>Validade:</b></label>
		<div class="input-group input-daterange">
		    <span class="input-group-text">De</span>
			<input class="form-control" type="date" name="validoDe" id="validoDe" value="{{isset($registro->validoDe) ? $registro->validoDe->format('Y-m-d') : '2000-01-01'}}">
		    <span class="input-group-text">Até</span>
			<input class="form-control" type="date" name="validoAte" id="validoAte" value="{{isset($registro->validoAte) ? $registro->validoAte->format('Y-m-d') : '2000-01-01'}}">
		</div>
	</div>
</div>