@section('logado', true)

@extends('layouts.site')

@section('titulo','Admin - Dashboard')

@section('conteudo')
<main>
    <div class="container-fluid">
        @include('layouts._includes.menssagem')
        <h1 class="mt-4">Tickets</h1>
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item"><a href="{{route('home')}}">Dashboard</a></li>
            <li class="breadcrumb-item active">Tickets</li>
        </ol>
        <div class="card mb-4">
            <div class="card-body">
                <div style="margin-bottom: 10px;">
                    <a href="{{route('tickets.adicionar')}}" title="Adicionar"><i class="fas fa-plus fa-2x text-success"></i></a>
                </div>
                <table class="table table-bordered" width="100%" cellspacing="0">
                    <thead class="thead-dark">
                        <tr>
                            <th scope="col" width="20">#</th>
                            <th scope="col" width="50">&nbsp;</th>
                            <th scope="col">Titulo</th>
                            <th scope="col" width="300">Código</th>
                            <th scope="col" width="300">Validade</th>
                            <th scope="col" width="100">Inclusão</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($registros as $i=>$registro)
                            <tr>
                                <td scope="row">{{++$i}}</td>
                                <td class="bg-dark">
                                    <div class="btn-group dropright">
                                        <button class="btn btn-link btn-sm order-1 order-lg-0" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-bars"></i></button>
                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton" style="width: 255px;">
                                            <a href="{{route('tickets.editar',$registro->id)}}" style="display: inline; margin-left: 20px; margin-right: 10px;" title="Editar"><i class="fas fa-edit text-success"></i></a>
                                            <a href="{{route('tickets.produtos',$registro->id)}}" style="display: inline; margin-left: 20px; margin-right: 10px;" title="Produtos"><i class="fas fa-cubes text-color-marrom"></i></a>
                                            <a href="{{route('tickets.marcas',$registro->id)}}" style="display: inline; margin-left: 20px; margin-right: 10px;" title="Marcas"><i class="fas fa-bookmark text-primary"></i></a>
                                            <a href="{{route('tickets.categorias',$registro->id)}}" style="display: inline; margin-left: 20px; margin-right: 10px;" title="Categorias"><i class="fas fa-stamp text-warning"></i></a>
                                            <a href="{{route('tickets.excluir',$registro->id)}}" style="display: inline; margin-left: 20px; margin-right: 10px;" title="Excluir"><i class="fas fa-trash text-danger"></i></a>
                                        </div>
                                    </div>
                                </td>
                                <td>{{$registro->titulo}}</td>
                                <td>{{$registro->codigo}}</td>
                                <td>{{date('d/m/Y', strtotime($registro->validoDe)).' até '.date('d/m/Y', strtotime($registro->validoAte))}}</td>
                                <td>{{$registro->dtInclusao->format('d/m/Y')}}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</main>
@endsection