<div class="form-group row">
	<div class="col-9">
		<label for="titulo" class="col-12 col-form-label"><b>Titulo:</b></label>
		<input class="form-control" type="text" name="titulo" id="titulo" value="{{isset($registro->titulo) ? $registro->titulo : ''}}">
	</div>
	<div class="col-3">
		<label for="codigo" class="col-12 col-form-label"><b>Código:</b></label>
		<input class="form-control" type="text" name="codigo" id="codigo" value="{{isset($registro->codigo) ? $registro->codigo : ''}}">
	</div>
</div>