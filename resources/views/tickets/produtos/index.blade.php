@section('logado', true)

@extends('layouts.site')

@section('titulo','Admin - Dashboard')

@section('conteudo')
<main>
    <div class="container-fluid">
        @include('layouts._includes.menssagem')
        <h1 class="mt-4">Tickets</h1>
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item"><a href="{{route('home')}}">Dashboard</a></li>
            <li class="breadcrumb-item"><a href="{{route('tickets')}}">Tickets</a></li>
            <li class="breadcrumb-item active">Produtos</li>
        </ol>
        <div class="card mb-4">
            <div class="card-body">
                <div class="row">
                    <div class="col-9">
                        <span style="font-size: 20px;"><b>Ticket: </b></span>{{$ticket->titulo.'('.$ticket->codigo.')'}}
                    </div>
                </div>
                <br>
                <br>
                <div style="margin-bottom: 10px;">
                    <a href="{{route('produtos', ['idTicket'=>$id])}}" title="Relacionar"><i class="fas fa-plus fa-2x text-success"></i></a>
                </div>
                <table class="table table-bordered" width="100%" cellspacing="0">
                    <thead class="thead-dark">
                        <tr>
                            <th scope="col" width="20">#</th>
                            <th scope="col" width="50">&nbsp;</th>
                            <th scope="col" width="200">Código</th>
                            <th scope="col">Produto</th>
                            <th scope="col" width="100">Inclusão</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($registros as $i=>$registro)
                            <tr>
                                <td scope="row">{{++$i}}</td>
                                <td class="bg-dark">
                                    <div class="btn-group dropright">
                                        <button class="btn btn-link btn-sm order-1 order-lg-0" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-bars"></i></button>
                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton" style="width: 55px;">
                                            <a href="{{route('tickets.produtos.excluir',$registro->id)}}" style="display: inline; margin-left: 20px; margin-right: 10px;" title="Excluir"><i class="fas fa-trash text-danger"></i></a>
                                        </div>
                                    </div>
                                </td>
                                <td>{{$registro->codigo}}</td>
                                <td>{{$registro->titulo}}</td>
                                <td>{{date('d/m/Y', strtotime($registro->dtInclusao))}}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</main>
@endsection