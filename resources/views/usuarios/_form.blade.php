<h4>Dados principais</h4>

<div class="form-group row">
	<div class="col-4">
		<label for="name" class="col-form-label"><b>Nome:</b></label>
		<input class="form-control" type="text" name="name" id="name" value="{{isset($registro->name) ? $registro->name : ''}}">
	</div>
	<div class="col-6">
		<label for="sobrenome" class="col-form-label"><b>Sobrenome:</b></label>
		<input class="form-control" type="text" name="sobrenome" id="sobrenome" value="{{isset($registro->sobrenome) ? $registro->sobrenome : ''}}">
	</div>
</div>

<!-- Area e cargo
<div class="form-group row">
	<div class="form-group col-sm-6" >
		<label for="idArea">*Área:</label>
		<select name="idArea" id="lstArea" required class="form-control selectpicker" data-live-search="true">
			<option value=""></option>
			<%
			rslista = fngetrows("select idArea, titulo from usuAreas where ativo = 1 order by titulo", cn)
			if isarray(rslista) then
				for r = 0 to ubound(rslista, 2)
					c = 0
					listaId = rslista(inc(c), r)
					listaTitulo = rslista(inc(c), r)
					%>
					<option value="<%= listaId %>"<% if listaId = idArea then %> selected<% end if %>><%= listaTitulo %></option>
					<%
				next
			end if
			%>
		</select>
	</div>
	<div class="form-group col-sm-6" >
		<label for="idCargo">*Cargo:</label>
		<select name="idCargo" id="lstCargo" required class="form-control selectpicker" data-live-search="true">
			<%
			if isvalidnumber(idArea) then
				%>
				<option value=""></option>
				<%
				rslista = fngetrows("select idCargo, titulo from usuCargos where idArea = " & idArea & " and ativo = 1 order by titulo", cn)
				if isarray(rslista) then
					for r = 0 to ubound(rslista, 2)
						c = 0
						listaId = rslista(inc(c), r)
						listaTitulo = rslista(inc(c), r)
						%>
						<option value="<%= listaId %>"<% if listaId = idCargo then %> selected<% end if %>><%= listaTitulo %></option>
						<%
					next
				end if
			else
				%>
				<option value=""></option>
				<option value="">Selecione uma área</option>
				<%
			end if
			%>
		</select>
	</div>
    <div class="clearfix"></div>
</div>
-->



<h4>Acesso ao sistema</h4>
<div class="form-group row">
	<div class="col-6">
		<label for="email" class="col-12 col-form-label"><b>Email:</b></label>
		<input class="form-control" type="email" name="email" id="email" aria-describedby="emailHelp" value="{{isset($registro->email) ? $registro->email : ''}}">
	</div>
	<div class="col-2">
		<label for="password" class="col-12 col-form-label"><b>Senha:</b></label>
		<input class="form-control" type="password" name="password" id="password" value="{{isset($registro->password) ? $registro->password : ''}}">
	</div>
</div>

<h4>Informações de contato</h4>
<div class="form-group row">
	<div class="col-6">
		<label for="endereco" class="col-12 col-form-label"><b>Endereço:</b></label>
		<input class="form-control" type="endereco" name="endereco" id="endereco" value="{{isset($registro->endereco) ? $registro->endereco : ''}}">
	</div>
	<div class="col-2">
		<label for="numero" class="col-12 col-form-label"><b>Número:</b></label>
		<input class="form-control" type="numero" name="numero" id="numero" value="{{isset($registro->numero) ? $registro->numero : ''}}">
	</div>
	<div class="col-4">
		<label for="complemento" class="col-12 col-form-label"><b>Complemento:</b></label>
		<input class="form-control" type="complemento" name="complemento" id="complemento" value="{{isset($registro->complemento) ? $registro->complemento : ''}}">
	</div>
	<div class="col-2">
		<label for="bairro" class="col-12 col-form-label"><b>Bairro:</b></label>
		<input class="form-control" type="bairro" name="bairro" id="bairro" value="{{isset($registro->bairro) ? $registro->bairro : ''}}">
	</div>
	<div class="col-2">
		<label for="cep" class="col-12 col-form-label"><b>CEP:</b></label>
		<input class="form-control" type="cep" name="cep" id="cep" value="{{isset($registro->cep) ? $registro->cep : ''}}">
	</div>
	<div class="col-2">
		<label for="estado" class="col-12 col-form-label"><b>Estado:</b></label>
		<input class="form-control" type="estado" name="estado" id="estado" value="{{isset($registro->estado) ? $registro->estado : ''}}">
	</div>
	<div class="col-6">
		<label for="cidade" class="col-12 col-form-label"><b>Cidade:</b></label>
		<input class="form-control" type="cidade" name="cidade" id="cidade" value="{{isset($registro->cidade) ? $registro->cidade : ''}}">
	</div>
	<div class="col-3">
		<label for="telCelular" class="col-12 col-form-label"><b>Tel. Celular:</b></label>
		<input class="form-control" type="telCelular" name="telCelular" id="telCelular" value="{{isset($registro->telCelular) ? $registro->telCelular : ''}}">
	</div>
	<div class="col-3">
		<label for="telFixo" class="col-12 col-form-label"><b>Tel. Fixo:</b></label>
		<input class="form-control" type="telFixo" name="telFixo" id="telFixo" value="{{isset($registro->telFixo) ? $registro->telFixo : ''}}">
	</div>
</div>

<h4>Informações financeiras</h4>
<div class="form-group row">
	<div class="col-3">
		<label for="cpf" class="col-12 col-form-label"><b>CPF:</b></label>
		<input class="form-control" type="cpf" name="cpf" id="cpf" value="{{isset($registro->cpf) ? $registro->cpf : ''}}">
	</div>
	<div class="col-3">
		<label for="banco" class="col-12 col-form-label"><b>Banco:</b></label>
		<input class="form-control" type="banco" name="banco" id="banco" value="{{isset($registro->banco) ? $registro->banco : ''}}">
	</div>
	<div class="col-2">
		<label for="agencia" class="col-12 col-form-label"><b>Agencia:</b></label>
		<input class="form-control" type="agencia" name="agencia" id="agencia" value="{{isset($registro->agencia) ? $registro->agencia : ''}}">
	</div>
	<div class="col-2">
		<label for="conta" class="col-12 col-form-label"><b>Conta:</b></label>
		<input class="form-control" type="conta" name="conta" id="conta" value="{{isset($registro->conta) ? $registro->conta : ''}}">
	</div>
</div>