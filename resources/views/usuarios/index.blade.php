@section('logado', true)

@extends('layouts.site')

@section('titulo','Admin - Dashboard')

@section('conteudo')
<main>
    <div class="container-fluid">
        @include('layouts._includes.menssagem')
        <h1 class="mt-4">Usuarios</h1>
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item"><a href="{{route('home')}}">Dashboard</a></li>
            <li class="breadcrumb-item active">Usuarios</li>
        </ol>
        <div class="card mb-4">
            <div class="card-body">
                @if(Session::get('permUsuAdmin'))
                <div style="margin-bottom: 10px;">
                    <a href="{{route('usuarios.adicionar')}}" title="Adicionar"><i class="fas fa-plus fa-2x text-success"></i></a>
                </div>
                @endif
                <table class="table table-bordered" width="100%" cellspacing="0">
                    <thead class="thead-dark">
                        <tr>
                            <th scope="col" width="20">#</th>
                            @if(Session::get('permUsuAdmin'))
                            <th scope="col" width="50">&nbsp;</th>
                            @endif
                            <th scope="col">Nome</th>
                            <th scope="col" width="300">E-mail</th>
                            <th scope="col" width="100">Inclusão</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php
                            $icons = 0
                        @endphp
                        @if(Session::get('permUsuPerm'))
                            @php
                                $icons = $icons + 1
                            @endphp
                        @endif
                        @if(Session::get('permUsuAdmin'))
                            @php
                                $icons = $icons + 2
                            @endphp
                        @endif
                        @foreach($registros as $i=>$registro)
                            <tr>
                                <td scope="row">{{++$i}}</td>
                                @if(Session::get('permUsuAdmin'))
                                <td class="bg-dark">
                                    <div class="btn-group dropright">
                                        <button class="btn btn-link btn-sm order-1 order-lg-0" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-bars"></i></button>
                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton" style="width:  {{55*$icons}}px;">
                                            <a href="{{route('usuarios.editar',$registro->id)}}" style="display: inline; margin-left: 20px; margin-right: 10px;" title="Editar"><i class="fas fa-edit text-success"></i></a>
                                            @if(Session::get('permUsuPerm'))
                                                <a href="{{route('usuarios.permissoes',$registro->id)}}" style="display: inline; margin-left: 20px; margin-right: 10px;" title="Permissões"><i class="fas fa-key text-warning"></i></a>
                                            @endif
                                            <a href="{{route('usuarios.excluir',$registro->id)}}" style="display: inline; margin-left: 20px; margin-right: 10px;" title="Excluir"><i class="fas fa-trash text-danger"></i></a>
                                        </div>
                                    </div>
                                </td>
                                @endif
                                <td>{{$registro->name." ".$registro->sobrenome}}</td>
                                <td>{{$registro->email}}</td>
                                <td>{{$registro->dtInclusao->format('d/m/Y')}}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</main>
@endsection