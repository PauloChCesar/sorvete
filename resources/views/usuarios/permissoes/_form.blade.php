<h4>Permissões</h4>

<div class="form-group row">
	<div class="col-4">
		<label for="marcarTodas" class="col-form-label"><input type="checkbox" name="marcarTodas" id="marcarTodas" value="1"> <b>Marcar todas</b></label>
	</div>
</div>
<div class="form-group row">
@foreach ($registros as $registro)
	<div class="col-4">
		<label for="permissoes[{{$registro->id}}]" class="col-form-label marcar"><input type="checkbox" name="permissoes[{{$registro->id}}]" id="permissoes[{{$registro->id}}]" value="{{$registro->id}}" @if ($registro->marcado) checked @endif> {{$registro->descricao}}</label>
	</div>
@endforeach
</div>