@section('logado', true)

@extends('layouts.site')

@section('titulo','Admin - Dashboard')

@section('conteudo')
<main>
    <div class="container-fluid">
        <h1 class="mt-4">Usuarios</h1>
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item"><a href="{{route('home')}}">Dashboard</a></li>
            <li class="breadcrumb-item"><a href="{{route('usuarios')}}">Usuarios</a></li>
            <li class="breadcrumb-item"><a href="{{route('usuarios.permissoes', $id)}}">Permissões</a></li>
            <li class="breadcrumb-item active">Editar</li>
        </ol>
        <div class="card mb-4">
            <div class="card-body">
                <form action="{{route('usuarios.permissoes.atualizar', ['id'=>$id,'idPerMod'=>$idPerMod])}}" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <input type="hidden" name="_method" value="put">
                    @include('usuarios.permissoes._form')
                    <button class="btn btn-success">Salvar</button>
                    <button class="btn btn-danger" type="Reset">Resetar</button>
                </form>
            </div>
        </div>
    </div>
</main>

<script type="text/javascript">

    document.addEventListener('DOMContentLoaded', function () {
        $("#marcarTodas").click(function(){
            $('input:checkbox').not(this).prop('checked', this.checked);
        })
    });
</script>
@endsection