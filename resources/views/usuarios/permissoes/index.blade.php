@section('logado', true)

@extends('layouts.site')

@section('titulo','Admin - Dashboard')

@section('conteudo')
<main>
    <div class="container-fluid">
        @include('layouts._includes.menssagem')
        <h1 class="mt-4">Usuarios</h1>
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item"><a href="{{route('home')}}">Dashboard</a></li>
            <li class="breadcrumb-item"><a href="{{route('usuarios')}}">Usuarios</a></li>
            <li class="breadcrumb-item active">Permissoes</li>
        </ol>
        <div class="card mb-4">
            <div class="card-body">
                <!--<div style="margin-bottom: 10px;">
                    <a href="{{route('usuarios.adicionar')}}" title="Adicionar"><i class="fas fa-plus fa-2x text-success"></i></a>
                </div>-->
                <table class="table table-bordered" width="100%" cellspacing="0">
                    <thead class="thead-dark">
                        <tr>
                            <th scope="col" width="20">#</th>
                            <th scope="col" width="50">&nbsp;</th>
                            <th scope="col">Módulo</th>
                            <th scope="col" width="50"><i class="fas fa-key" title="Permissoes"></i></th>
                        </tr>
                    </thead>
                    <tbody>
                        @php
                            $icons = 1
                        @endphp
                        @foreach($registros as $i=>$registro)
                            <tr>
                                <td scope="row">{{++$i}}</td>
                                <td class="bg-dark">
                                    <div class="btn-group dropright">
                                        <button class="btn btn-link btn-sm order-1 order-lg-0" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-bars"></i></button>
                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton" style="width:  {{55*$icons}}px;">
                                            <a href="{{route('usuarios.permissoes.editar',['id'=>$registro->idUS,'idPerMod'=>$registro->idPerMod])}}" style="display: inline; margin-left: 20px; margin-right: 10px;" title="Editar"><i class="fas fa-edit text-success"></i></a>
                                        </div>
                                    </div>
                                </td>
                                <td>{{$registro->descPermMod}}</td>
                                <td>{{$registro->qtdeAlt}}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</main>
@endsection