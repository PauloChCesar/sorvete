<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('auth/login');
});

Auth::routes();

Route::get('/home', ['as' => 'home', 'uses' => 'HomeController@index']);

//Rotas de Usuarios
Route::get('/usuarios', ['as'=>'usuarios','uses'=>'UsuarioController@index']);
Route::get('/usuarios/adicionar', ['as'=>'usuarios.adicionar','uses'=>'UsuarioController@adicionar']);
Route::post('/usuarios/salvar', ['as'=>'usuarios.salvar','uses'=>'UsuarioController@salvar']);
Route::get('/usuarios/editar/{id}', ['as'=>'usuarios.editar','uses'=>'UsuarioController@editar']);
Route::put('/usuarios/atualizar/{id}', ['as'=>'usuarios.atualizar','uses'=>'UsuarioController@atualizar']);
Route::get('/usuarios/excluir/{id}', ['as'=>'usuarios.excluir','uses'=>'UsuarioController@excluir']);
Route::get('/usuarios/permissoes/{id}', ['as'=>'usuarios.permissoes','uses'=>'UsuarioController@permissoes']);
Route::get('/usuarios/permissoes/editar/{id}/{idPerMod}', ['as'=>'usuarios.permissoes.editar','uses'=>'UsuarioController@permissoesEditar']);
Route::put('/usuarios/permissoes/atualizar/{id}/{idPerMod}', ['as'=>'usuarios.permissoes.atualizar','uses'=>'UsuarioController@permissoesAtualizar']);

//Rotas de Clientes
Route::get('/clientes', ['as'=>'clientes','uses'=>'ClienteController@index']);
Route::get('/clientes/adicionar', ['as'=>'clientes.adicionar','uses'=>'ClienteController@adicionar']);
Route::post('/clientes/salvar', ['as'=>'clientes.salvar','uses'=>'ClienteController@salvar']);
Route::get('/clientes/editar/{id}', ['as'=>'clientes.editar','uses'=>'ClienteController@editar']);
Route::put('/clientes/atualizar/{id}', ['as'=>'clientes.atualizar','uses'=>'ClienteController@atualizar']);
Route::get('/clientes/excluir/{id}', ['as'=>'clientes.excluir','uses'=>'ClienteController@excluir']);

//Rotas de Depoimentos
Route::get('/depoimentos', ['as'=>'depoimentos','uses'=>'DepoimentoController@index']);
Route::get('/depoimentos/adicionar', ['as'=>'depoimentos.adicionar','uses'=>'DepoimentoController@adicionar']);
Route::post('/depoimentos/salvar', ['as'=>'depoimentos.salvar','uses'=>'DepoimentoController@salvar']);
Route::get('/depoimentos/editar/{id}', ['as'=>'depoimentos.editar','uses'=>'DepoimentoController@editar']);
Route::put('/depoimentos/atualizar/{id}', ['as'=>'depoimentos.atualizar','uses'=>'DepoimentoController@atualizar']);
Route::get('/depoimentos/excluir/{id}', ['as'=>'depoimentos.excluir','uses'=>'DepoimentoController@excluir']);

//Rotas de Duvidas
Route::get('/duvidas', ['as'=>'duvidas','uses'=>'DuvidaController@index']);
Route::get('/duvidas/adicionar', ['as'=>'duvidas.adicionar','uses'=>'DuvidaController@adicionar']);
Route::post('/duvidas/salvar', ['as'=>'duvidas.salvar','uses'=>'DuvidaController@salvar']);
Route::get('/duvidas/editar/{id}', ['as'=>'duvidas.editar','uses'=>'DuvidaController@editar']);
Route::put('/duvidas/atualizar/{id}', ['as'=>'duvidas.atualizar','uses'=>'DuvidaController@atualizar']);
Route::get('/duvidas/excluir/{id}', ['as'=>'duvidas.excluir','uses'=>'DuvidaController@excluir']);

//Rotas de Produtos
Route::get('/produtos', ['as'=>'produtos','uses'=>'ProdutoController@index']);
Route::get('/produtos/adicionar', ['as'=>'produtos.adicionar','uses'=>'ProdutoController@adicionar']);
Route::post('/produtos/salvar', ['as'=>'produtos.salvar','uses'=>'ProdutoController@salvar']);
Route::get('/produtos/editar/{id}', ['as'=>'produtos.editar','uses'=>'ProdutoController@editar']);
Route::put('/produtos/atualizar/{id}', ['as'=>'produtos.atualizar','uses'=>'ProdutoController@atualizar']);
Route::get('/produtos/excluir/{id}', ['as'=>'produtos.excluir','uses'=>'ProdutoController@excluir']);

//Rotas de Produtos configurações
Route::get('/produtos/configuracoes/{id}', ['as'=>'produtos.configuracoes','uses'=>'ConfiguracaoController@index']);
Route::get('/produtos/configuracoes/registros/{id}', ['as'=>'produtos.configuracoes.registros','uses'=>'ConfiguracaoController@registros']);
Route::get('/produtos/configuracoes/adicionar/{id}', ['as'=>'produtos.configuracoes.registros.adicionar','uses'=>'ConfiguracaoController@adicionar']);
Route::post('/produtos/configuracoes/salvar/{id}', ['as'=>'produtos.configuracoes.registros.salvar','uses'=>'ConfiguracaoController@salvar']);
Route::get('/produtos/configuracoes/editar/{id}', ['as'=>'produtos.configuracoes.registros.editar','uses'=>'ConfiguracaoController@editar']);
Route::put('/produtos/configuracoes/atualizar/{id}', ['as'=>'produtos.configuracoes.registros.atualizar','uses'=>'ConfiguracaoController@atualizar']);
Route::get('/produtos/configuracoes/excluir/{id}', ['as'=>'produtos.configuracoes.registros.excluir','uses'=>'ConfiguracaoController@excluir']);

//Rotas de Produtos Imagens
Route::get('/produtos/imagens/{id}', ['as'=>'produtos.imagens','uses'=>'ProdutoController@indexImagem']);
Route::get('/produtos/imagens/adicionar/{id}', ['as'=>'produtos.imagens.adicionar','uses'=>'ProdutoController@adicionarImagem']);
Route::post('/produtos/imagens/salvar/{id}', ['as'=>'produtos.imagens.salvar','uses'=>'ProdutoController@salvarImagem']);
Route::get('/produtos/imagens/excluir/{id}/{idImagem}', ['as'=>'produtos.imagens.excluir','uses'=>'ProdutoController@excluirImagem']);

//Rotas de Categorias
Route::get('/categorias', ['as'=>'categorias','uses'=>'CategoriaController@index']);
Route::get('/categorias/adicionar', ['as'=>'categorias.adicionar','uses'=>'CategoriaController@adicionar']);
Route::post('/categorias/salvar', ['as'=>'categorias.salvar','uses'=>'CategoriaController@salvar']);
Route::get('/categorias/editar/{id}', ['as'=>'categorias.editar','uses'=>'CategoriaController@editar']);
Route::put('/categorias/atualizar/{id}', ['as'=>'categorias.atualizar','uses'=>'CategoriaController@atualizar']);
Route::get('/categorias/excluir/{id}', ['as'=>'categorias.excluir','uses'=>'CategoriaController@excluir']);

//Rotas de Marcas
Route::get('/marcas', ['as'=>'marcas','uses'=>'MarcaController@index']);
Route::get('/marcas/adicionar', ['as'=>'marcas.adicionar','uses'=>'MarcaController@adicionar']);
Route::post('/marcas/salvar', ['as'=>'marcas.salvar','uses'=>'MarcaController@salvar']);
Route::get('/marcas/editar/{id}', ['as'=>'marcas.editar','uses'=>'MarcaController@editar']);
Route::put('/marcas/atualizar/{id}', ['as'=>'marcas.atualizar','uses'=>'MarcaController@atualizar']);
Route::get('/marcas/excluir/{id}', ['as'=>'marcas.excluir','uses'=>'MarcaController@excluir']);

//Rotas de Analises
Route::get('/analises/{idProduto}', ['as'=>'analises','uses'=>'AnaliseController@index']);
Route::get('/analises/adicionar/{idProduto}', ['as'=>'analises.adicionar','uses'=>'AnaliseController@adicionar']);
Route::post('/analises/salvar', ['as'=>'analises.salvar','uses'=>'AnaliseController@salvar']);
Route::get('/analises/editar/{id}', ['as'=>'analises.editar','uses'=>'AnaliseController@editar']);
Route::put('/analises/atualizar/{id}', ['as'=>'analises.atualizar','uses'=>'AnaliseController@atualizar']);
Route::get('/analises/excluir/{id}', ['as'=>'analises.excluir','uses'=>'AnaliseController@excluir']);

//Rotas de Tickets
Route::get('/tickets', ['as'=>'tickets','uses'=>'TicketController@index']);
Route::get('/tickets/adicionar', ['as'=>'tickets.adicionar','uses'=>'TicketController@adicionar']);
Route::post('/tickets/salvar', ['as'=>'tickets.salvar','uses'=>'TicketController@salvar']);
Route::get('/tickets/editar/{id}', ['as'=>'tickets.editar','uses'=>'TicketController@editar']);
Route::put('/tickets/atualizar/{id}', ['as'=>'tickets.atualizar','uses'=>'TicketController@atualizar']);
Route::get('/tickets/excluir/{id}', ['as'=>'tickets.excluir','uses'=>'TicketController@excluir']);

//Rotas de Tickets produtos
Route::get('/tickets/produtos/{id}', ['as'=>'tickets.produtos','uses'=>'TicketController@indexProdutos']);
Route::get('/tickets/produtos/salvar/{id}/{idProduto}', ['as'=>'tickets.produtos.salvar','uses'=>'TicketController@salvarProdutos']);
Route::get('/tickets/produtos/excluir/{id}', ['as'=>'tickets.produtos.excluir','uses'=>'TicketController@excluirProdutos']);

//Rotas de Tickets marcas
Route::get('/tickets/marcas', ['as'=>'tickets.marcas','uses'=>'TicketController@indexMarcas']);
Route::get('/tickets/marcas/adicionar', ['as'=>'tickets.marcas.adicionar','uses'=>'TicketController@adicionarMarcas']);
Route::post('/tickets/marcas/salvar', ['as'=>'tickets.marcas.salvar','uses'=>'TicketController@salvarMarcas']);
Route::get('/tickets/marcas/editar/{id}', ['as'=>'tickets.marcas.editar','uses'=>'TicketController@editarMarcas']);
Route::put('/tickets/marcas/atualizar/{id}', ['as'=>'tickets.marcas.atualizar','uses'=>'TicketController@atualizarMarcas']);
Route::get('/tickets/marcas/excluir/{id}', ['as'=>'tickets.marcas.excluir','uses'=>'TicketController@excluirMarcas']);

//Rotas de Tickets categorias
Route::get('/tickets/categorias', ['as'=>'tickets.categorias','uses'=>'TicketController@indexCategorias']);
Route::get('/tickets/categorias/adicionar', ['as'=>'tickets.categorias.adicionar','uses'=>'TicketController@adicionarCategorias']);
Route::post('/tickets/categorias/salvar', ['as'=>'tickets.categorias.salvar','uses'=>'TicketController@salvarCategorias']);
Route::get('/tickets/categorias/editar/{id}', ['as'=>'tickets.categorias.editar','uses'=>'TicketController@editarCategorias']);
Route::put('/tickets/categorias/atualizar/{id}', ['as'=>'tickets.categorias.atualizar','uses'=>'TicketController@atualizarCategorias']);
Route::get('/tickets/categorias/excluir/{id}', ['as'=>'tickets.categorias.excluir','uses'=>'TicketController@excluirCategorias']);

//Rotas de Vendas
Route::get('/vendas', ['as'=>'vendas','uses'=>'VendaController@index']);
Route::get('/vendas/adicionar', ['as'=>'vendas.adicionar','uses'=>'VendaController@adicionar']);
Route::post('/vendas/salvar', ['as'=>'vendas.salvar','uses'=>'VendaController@salvar']);
Route::get('/vendas/editar/{id}', ['as'=>'vendas.editar','uses'=>'VendaController@editar']);
Route::put('/vendas/atualizar/{id}', ['as'=>'vendas.atualizar','uses'=>'VendaController@atualizar']);
Route::get('/vendas/excluir/{id}', ['as'=>'vendas.excluir','uses'=>'VendaController@excluir']);